<?php 


	$connexion=getConnexionBD(); // connexion à la BD
	mysqli_set_charset($connexion, "utf8");
	$requete='SELECT * FROM dataset.DonneesFournies WHERE type=\'créature\'';
	$reponse=mysqli_query($connexion, $requete);
	if(!mysqli_query($connexion, $requete)){
		printf("Erreur : %s\n", mysqli_error($connexion));
	}
	$donnees=mysqli_fetch_all($reponse,MYSQLI_ASSOC);
	foreach($donnees as $ligne)
	{
		$id=$ligne['id'];
		$nom=mysqli_real_escape_string($connexion, $ligne['nom']);
		$attributs=explode(',', $ligne['attributs']);
		parse_str($ligne['attributs'], $output);
		$categorie=$output['catégorie'];
		parse_str($attributs[1], $output);
		$climat=$output['climat'];
		parse_str($attributs[2], $output);
		$PO=$output['pieces'];
		parse_str($attributs[3], $output);
		$environnement=$output['environnement'];
		parse_str($attributs[4], $output);
		$difficulte=$output['difficulté'];
		parse_str($attributs[5], $output);
		$AP=$output['attaque'];
		parse_str($attributs[6], $output);
		$HP=$output['vie'];
		mysqli_real_escape_string($connexion,$nom);
				
		$req="INSERT INTO EtreRencontre(IdEtre, PO, HP, AP) VALUES ($id , $PO, $HP, $AP)";
		$test=mysqli_query($connexion, $req);
		
		$req8="INSERT INTO Environnement(NomEnvironnement) VALUES ('$environnement')";
		$test8=mysqli_query($connexion, $req8);	
		
		$req2="INSERT INTO Creature(IdEtre,IdEnvironnement, NomCreature, Climat, NiveauCreature) VALUES ($id,(SELECT IdEnvironnement FROM Environnement WHERE NomEnvironnement='$environnement'), '$nom', '$climat',$difficulte)";
		$test2=mysqli_query($connexion, $req2);
		
	}
		
	
	$requete2='SELECT * FROM dataset.DonneesFournies WHERE type=\'piège\'';
	$reponse2=mysqli_query($connexion, $requete2);
	$donnees2=mysqli_fetch_all($reponse2,MYSQLI_ASSOC);
	foreach($donnees2 as $ligne2)
	{
		$id=$ligne2['id'];
		$nom=mysqli_real_escape_string($connexion, $ligne2['nom']);
		$attributs=explode(',', $ligne2['attributs']);
		parse_str($attributs[0], $output);
		$categorie=$output['catégorie'];
		parse_str($attributs[1], $output);
		$detecter=$output['detecter'];
		parse_str($attributs[2], $output);
		$esquiver=$output['esquiver'];
		parse_str($attributs[3], $output);
		$desamorcer=$output['desamorcer'];
		parse_str($attributs[4], $output);
		$zone=$output['zone'];
		$dim=explode('x', $zone);
		parse_str($attributs[5], $output);
		$image = $output['image'];
		$larg=substr("$dim[1]",0,1);
		if(!is_int($dim[0]))
		{
			$dim[0]=0;
		}
		if(!is_int($larg))
		{
			$larg=0;
		}
		
		
		$req3="INSERT INTO Element(IdElement, NomElement, ImageElement, LongueurElement, LargeurElement) VALUES ($id, '$nom', '$image', $dim[0], '$larg')";
		$test3=mysqli_query($connexion, $req3);
	
		$req4="INSERT INTO Piege(IdElement, CategoriePiege, DifficulteDetection, DifficulteEsquive, DifficulteDesamorcage) VALUES ($id, '$categorie', '$detecter', '$esquiver', '$desamorcer')";
		$test4=mysqli_query($connexion, $req4);
		
	}
		
	$requete3='SELECT * FROM dataset.DonneesFournies WHERE type=\'mobilier\'';
	$reponse3=mysqli_query($connexion, $requete3);
	$donnees3=mysqli_fetch_all($reponse3,MYSQLI_ASSOC);
	foreach($donnees3 as $ligne3)
	{
		$id=$ligne3['id'];
		$nom=mysqli_real_escape_string($connexion, $ligne3['nom']);
		$attributs=explode(',', $ligne3['attributs']);
		parse_str($attributs[0], $output);
		$image=$output['image'];
		parse_str($attributs[1], $output);
		$deplacable=$output['deplacable'];
		parse_str($attributs[2], $output);
		$dimensions=$output['dimensions'];
		$dim=explode('x', $dimensions);
		
		
		$req5="INSERT INTO Element(IdElement, NomElement, ImageElement, LongueurElement, LargeurElement) VALUES ($id, '$nom', '$image', $dim[0], $dim[1])";
		$test5=mysqli_query($connexion, $req5);
		
		
		$req6="INSERT INTO Mobilier(IdElement, NomMobilier, Deplacable) VALUES ($id, '$nom', '$deplacable')";
		$test6=mysqli_query($connexion, $req6);
		
		
	}
	?>
