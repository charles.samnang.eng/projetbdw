<?php

/*
** Il est possible d'automatiser le routing, notamment en cherchant directement le fichier controleur et le fichier vue.
** ex, pour page=afficher : verification de l'existence des fichiers controleurs/controleurAfficher.php et vues/vueAfficher.php
** Cela impose un nommage strict des fichiers.
*/

$routes = array(
	'jouer' => array('controleur' => 'controleurJouer', 'vue' => 'vueJouer'),
	'description' => array('controleur' => 'controleurDescription', 'vue' => 'vueDescription'),
	'regles' => array('controleur' => 'controleurRegles', 'vue' => 'vueRegles'),
	'carte' => array('controleur' => 'controleurCarte', 'vue' => 'vueCarte'),
	'cartescrees' => array('controleur' => 'controleurCartescrees', 'vue' => 'vueCartescrees'),
	'recherche' => array('controleur' => 'controleurRecherche', 'vue' => 'vueRecherche'),
	'zone' => array('controleur' => 'controleurZone', 'vue' => 'vueZone'),
	'script' => array('controleur' => 'controleurScript', 'vue' => 'vueScript')
);

?>
