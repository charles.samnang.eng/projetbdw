-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- ---------------------------------------------------

--
-- Structure de la table `JeuDePlateau`
--
CREATE TABLE `JeuDePlateau` (
  `IdJeu` INT UNSIGNED,
    `IdCarte` INT UNSIGNED,
  `NomJeu` VARCHAR(50),
  PRIMARY KEY (`IdJeu`)
)ENGINE=InnoDB;

--
-- Structure de la table `Carte`
--
CREATE TABLE `Carte` (
  `IdCarte` INT UNSIGNED AUTO_INCREMENT,
    `IdJeu` INT UNSIGNED,
    `IdContributrice` INT UNSIGNED,
    `IdZone` INT UNSIGNED,
    `IdParametre` INT UNSIGNED,
  `NomCarte` VARCHAR(50),
  `DescriptionCarte` VARCHAR(250),
  `DateCreationCarte` DATE,
  PRIMARY KEY (`IdCarte`)
)ENGINE=InnoDB;

--
-- Structure de la table `Contributrice`
--
CREATE TABLE `Contributrice` (
  `IdContributrice` INT UNSIGNED,
    `IdCarte` INT UNSIGNED,
  `NomContributrice` VARCHAR(50),
  `PrenomContributrice` VARCHAR(50),
  `DateInscriptionContributrice` DATE,
  PRIMARY KEY (`IdContributrice`)
)ENGINE=InnoDB;

--
-- Structure de la table `DateModif`
--
CREATE TABLE `DateModif` (
  `DateModification` DATE,
    `IdCarte` INT UNSIGNED,
    `IdContributrice` INT UNSIGNED,
  PRIMARY KEY (`DateModification`)
)ENGINE=InnoDB;

--
-- Structure de la table `Objectif`
--
CREATE TABLE `Objectif` (
  `IdObjectif` INT UNSIGNED,
    `IdCarte` INT UNSIGNED,
  `NomObjectif` VARCHAR(50),
  `DescriptionObjectif` VARCHAR(250),
  PRIMARY KEY (`IdObjectif`)
)ENGINE=InnoDB;

--
-- Structure de la table `Parametre`
--
CREATE TABLE `Parametre` (
  `IdParametre` INT UNSIGNED,
    `IdCarte` INT UNSIGNED,
  `NomParametre` VARCHAR(50),
  `ValeurParametre` INTEGER,
  PRIMARY KEY (`IdParametre`)
)ENGINE=InnoDB;

--
-- Structure de la table `DateParametre`
--
CREATE TABLE `DateParametre` (
  `DateParametre` DATE,
    `IdCarte` INT UNSIGNED,
    `IdParametre` INT UNSIGNED,
  PRIMARY KEY (`DateParametre`)
)ENGINE=InnoDB;

--
-- Structure de la table `Zone`
--
CREATE TABLE `Zone` (
  `IdZone` INT UNSIGNED NULL,
    `IdCarte` INT UNSIGNED,
    `IdElement` INT UNSIGNED,
    `IdEtre` INT UNSIGNED,
    `IdEnvironnement` INT UNSIGNED,
  `NomZone` VARCHAR(50),
  `DescriptionZone` VARCHAR(250),
  `LongueurZone` INTEGER UNSIGNED,
  `LargeurZone` INTEGER UNSIGNED,
  PRIMARY KEY (`IdZone`)
)ENGINE=InnoDB;

--
-- Structure de la table `Environnement`
--
CREATE TABLE `Environnement` (
  `IdEnvironnement` INT UNSIGNED AUTO_INCREMENT,
    `IdZone` INT UNSIGNED,
  `NomEnvironnement` VARCHAR(50) UNIQUE,
  `DescriptionEnvironnement` VARCHAR(250),
  PRIMARY KEY (`IdEnvironnement`)
)ENGINE=InnoDB;

--
-- Structure de la table `EtreRencontre`
--
CREATE TABLE `EtreRencontre` (
  `IdEtre` INT UNSIGNED,
    `IdZone` INT UNSIGNED,
  `PO` INTEGER,
  `HP` INTEGER,
  `AP` INTEGER,
  `PositionXEtre` INTEGER,
  `PositionYEtre` INTEGER,
  PRIMARY KEY (`IdEtre`)
)ENGINE=InnoDB;

--
-- Structure de la table `Creature`
--
CREATE TABLE `Creature` (
    `IdEtre` INT UNSIGNED,
    `IdEnvironnement` INT UNSIGNED,
   `CategorieCreature` VARCHAR(50),
  `NomCreature` VARCHAR(50),
  `Climat` VARCHAR(50),
  `NiveauCreature` INTEGER UNSIGNED,
  PRIMARY KEY (`IdEtre`)
)ENGINE=InnoDB;

--
-- Structure de la table `PNJ`
--
CREATE TABLE `PNJ` (
    `IdEtre` INT UNSIGNED,
  `NomPNJ` VARCHAR(50),
  `MetierPNJ` VARCHAR(50),
  `TraitPNJ` VARCHAR(50),
  `PhrasePNJ` VARCHAR(1000),
  PRIMARY KEY (`IdEtre`)

)ENGINE=InnoDB;

--
-- Structure de la table `Element`
--
CREATE TABLE `Element` (
  `IdElement` INT UNSIGNED,
    `IdZone` INT UNSIGNED,
    `IdPassageSecret` INT UNSIGNED,
  `NomElement` VARCHAR(100),
  `ImageElement` VARCHAR(50),
  `LongueurElement` INTEGER UNSIGNED,
  `LargeurElement` INTEGER UNSIGNED,
  PRIMARY KEY (`IdElement`)
)ENGINE=InnoDB;

--
-- Structure de la table `Mobilier`
--
CREATE TABLE `Mobilier` (
    `IdElement` INT UNSIGNED,
  `NomMobilier` VARCHAR(50), 
  `Deplacable` VARCHAR(50),
  PRIMARY KEY (`IdElement`)
)ENGINE=InnoDB;

--
-- Structure de la table `Equipement`
--
CREATE TABLE `Equipement` (
    `IdElement` INT UNSIGNED,
  `ValeurMonetaire` INTEGER UNSIGNED,
  PRIMARY KEY (`IdElement`)
)ENGINE=InnoDB;

--
-- Structure de la table `Piege`
--
CREATE TABLE `Piege` (
    `IdElement` INT UNSIGNED,
  `CategoriePiege` VARCHAR(50),
  `DifficulteDetection` VARCHAR(50),
  `DifficulteEsquive` VARCHAR(50),
  `DifficulteDesamorcage` VARCHAR(50),
  PRIMARY KEY (`IdElement`)
)ENGINE=InnoDB;

--
-- Structure de la table `PassageSecret`
--
CREATE TABLE `PassageSecret` (
  `IdPassageSecret` INT UNSIGNED,
    `IdElement` INT UNSIGNED,
  `DifficulteDetection` VARCHAR(50),
  `PositionXTeleportation` INTEGER,
  `PositionYTeleportation` INTEGER,
  PRIMARY KEY (`IdPassageSecret`)
)ENGINE=InnoDB;


-- -------------------------------------------------

--
-- Index pour la table `Carte`
--
ALTER TABLE `Carte`
  ADD KEY `IdContributrice` (`IdContributrice`),
  ADD KEY `IdParametre` (`IdParametre`);

--
-- Index pour la table `DateModif`
--
ALTER TABLE `DateModif`
  ADD KEY `IdContributrice` (`IdContributrice`),
  ADD KEY `IdCarte` (`IdCarte`);

--
-- Index pour la table `Parametre`
--
ALTER TABLE `Parametre`
  ADD KEY `IdCarte` (`IdCarte`);

--
-- Index pour la table `DateParametre`
--
ALTER TABLE `DateParametre`
  ADD KEY `IdParametre` (`IdParametre`),
  ADD KEY `IdCarte` (`IdCarte`);

--
-- Index pour la table `Zone`
--
ALTER TABLE `Zone`
  ADD KEY `IdElement` (`IdElement`),
  ADD KEY `IdEtre` (`IdEtre`),
  ADD KEY `IdEnvironnement` (`IdEnvironnement`);

--
-- Index pour la table `Creature`
--
ALTER TABLE `Creature`
  ADD KEY `IdEtre` (`IdEtre`),
  ADD KEY `IdEnvironnement` (`IdEnvironnement`);

--
-- Index pour la table `PNJ`
--
ALTER TABLE `PNJ`
  ADD KEY `IdEtre` (`IdEtre`);

--
-- Index pour la table `Element`
--
ALTER TABLE `Element`
  ADD KEY `IdPassageSecret` (`IdPassageSecret`);

--
-- Index pour la table `Mobilier`
--
ALTER TABLE `Mobilier`
  ADD KEY `IdElement` (`IdElement`);

--
-- Index pour la table `Equipement`
--
ALTER TABLE `Equipement`
  ADD KEY `IdElement` (`IdElement`);

--
-- Index pour la table `Piege`
--
ALTER TABLE `Piege`
  ADD KEY `IdElement` (`IdElement`);

--
-- Index pour la table `PassageSecret`
--
ALTER TABLE `PassageSecret`
  ADD KEY `IdElement` (`IdElement`);

-- ----------------------------------------------------------------

--
-- Contraintes pour la table `Carte`
--
ALTER TABLE `Carte`
  ADD CONSTRAINT `fk.IdContributriceOfCarte` FOREIGN KEY (`IdContributrice`) REFERENCES Contributrice(`IdContributrice`),
  ADD CONSTRAINT `fk.IdParametreOfCarte` FOREIGN KEY (`IdParametre`) REFERENCES Parametre(`IdParametre`);

--
-- Contraintes pour la table `DateModif`
--
ALTER TABLE `DateModif`
  ADD  CONSTRAINT `fk.IdCarteOfDate_Modif` FOREIGN KEY (`IdCarte`) REFERENCES Carte(`IdCarte`),
  ADD CONSTRAINT `fk.IdContributrice` FOREIGN KEY (`IdContributrice`) REFERENCES Contributrice(`IdContributrice`);

--
-- Contraintes pour la table `Parametre`
--
ALTER TABLE `Parametre`
  ADD CONSTRAINT `fk.IdCarteOfParametre` FOREIGN KEY (`IdCarte`) REFERENCES Carte(`IdCarte`);

--
-- Contraintes pour la table `DateParametre`
--
ALTER TABLE `DateParametre`
  ADD CONSTRAINT `fk.IdCarteOfDate_Parametre` FOREIGN KEY (`IdCarte`) REFERENCES Carte(`IdCarte`),
  ADD CONSTRAINT `fk.IdParametreOfDate_Parametre` FOREIGN KEY (`IdParametre`) REFERENCES Parametre(`IdParametre`);

--
-- Contraintes pour la table `Zone`
--
ALTER TABLE `Zone`
  ADD CONSTRAINT `fk.IdElementOfZone` FOREIGN KEY(`IdElement`) REFERENCES Element(`IdElement`),
  ADD CONSTRAINT `fk.IdEtreOfZone` FOREIGN KEY(`IdEtre`) REFERENCES EtreRencontre(`IdEtre`),
  ADD CONSTRAINT `fk.IdEnvironnementOfZone` FOREIGN KEY(`IdEnvironnement`) REFERENCES Environnement(`IdEnvironnement`);

--
-- Contraintes pour la table `Creature`
--
ALTER TABLE `Creature`
  ADD CONSTRAINT `fk.IdEtreOfCreature` FOREIGN KEY(`IdEtre`) REFERENCES EtreRencontre(`IdEtre`),
  ADD CONSTRAINT `fk.IdEnvironnementOfCreature` FOREIGN KEY(`IdEnvironnement`) REFERENCES Environnement(`IdEnvironnement`);

--
-- Contraintes pour la table `PNJ`
--
ALTER TABLE `PNJ`
  ADD CONSTRAINT `fk.IdEtreOfPNJ` FOREIGN KEY(`IdEtre`) REFERENCES EtreRencontre(`IdEtre`);

--
-- Contraintes pour la table `Element`
--
ALTER TABLE `Element`
  ADD CONSTRAINT `fk.IdPassageSecretOfElement` FOREIGN KEY (`IdPassageSecret`) REFERENCES PassageSecret(`IdPassageSecret`);

--
-- Contraintes pour la table `Mobilier`
--
ALTER TABLE `Mobilier`
  ADD CONSTRAINT `fk.IdElementOfMobilier` FOREIGN KEY (`IdElement`) REFERENCES Element(`IdElement`);

--
-- Contraintes pour la table `Equipement`
--
ALTER TABLE `Equipement`
  ADD CONSTRAINT `fk.IdElementOfEquipement` FOREIGN KEY (`IdElement`) REFERENCES Element(`IdElement`);

--
-- Contraintes pour la table `Piege`
--
ALTER TABLE `Piege`
  ADD CONSTRAINT `fk.IdElementOfPiege` FOREIGN KEY (`IdElement`) REFERENCES Element(`IdElement`);

--
-- Contraintes pour la table `PassageSecret`
--
ALTER TABLE `PassageSecret`
  ADD CONSTRAINT `fk.IdElementOfPassage` FOREIGN KEY (`IdElement`) REFERENCES Element(`IdElement`);

COMMIT;
