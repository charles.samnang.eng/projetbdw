<!DOCTYPE html>
<!-- 
Page d'accueil non MVC. En début de TP, on vous demande de la modifier directement, puis vous la transformerez selon l'architecture MVC.
-->
<html>
<head>
    <meta charset="utf-8" />
    <!-- lie le style CSS externe  -->
    <link href="css/style.css" rel="stylesheet" media="all" type="text/css">
    <!-- ajoute une image favicon (dans l'onglet du navigateur) -->
</head>
<body>
	<header><?php include('static/header.php'); ?></header>
    <br />
    <div id="divCentral">
		<?php include('static/menu.php'); ?>
		<main>
			<form method="post" action="">
				<p>
			<select name="choix">
				<option value="créature">Créatures</option>
				<option value="piège">Pièges</option>
				<option value="mobilier">Mobiliers</option>
			</select>
			
			<input type="submit" value="Valider" name="done" />
			
			</p>
			
			</form>
<?php 
if(isset($_POST['done']))
{
 if($_POST['choix']=='créature')
 {
		$connexion=getConnexionBD();
		$requete='SELECT * FROM Creature';
		$reponse=mysqli_query($connexion, $requete);
		if($reponse == FALSE){
			printf("<p>Un problème est survenu lors de la récupération des créatures.</p>");
		}
		else {
			echo '<h2>Liste des créatures</h2><p><ul>';
			echo '<table border="1" width="400">';
			echo '<tr>';
			echo '<td>Id Etre</td>';
			echo '<td>Id Environnement</td>';
			echo '<td>Nom Creature</td>';
			echo '<td>Climat</td>';
			echo '<td>Niveau Creature</td>';
			echo '</tr>';
			
			while ($row = mysqli_fetch_assoc($reponse)) {
				echo '<tr>';
				echo '<td>';
				echo  $row['IdEtre'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['IdEnvironnement'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['NomCreature'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['Climat'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['NiveauCreature'] ;
				echo '</td>';
				echo '</tr>';
			}
			echo '</ul></p>';
			echo '</table>';
		}
	 
 } 
 
 if($_POST['choix']=='piège')
 {
		$connexion=getConnexionBD();
		$requete='SELECT * FROM Piege';
		$reponse=mysqli_query($connexion, $requete);
		if($reponse == FALSE){
			printf("<p>Un problème est survenu lors de la récupération des pièges.</p>");
		}
		else {
			echo '<h2>Liste des pièges</h2><p><ul>';
			echo '<table border="1" width="400">';
			echo '<tr>';
			echo '<td>Id Element</td>';
			echo '<td>Catégorie piège</td>';
			echo '<td>Difficulté Détection</td>';
			echo '<td>Difficulté Esquive</td>';
			echo '<td>Difficulté Désamorçage</td>';
			echo '</tr>';
			
			while ($row = mysqli_fetch_assoc($reponse)) {
				echo '<tr>';
				echo '<td>';
				echo  $row['IdElement'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['CategoriePiege'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['DifficulteDetection'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['DifficulteEsquive'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['DifficulteDesamorcage'] ;
				echo '</td>';
				echo '</tr>';
			}
			echo '</ul></p>';
			echo '</table>';
		}
	 
 } 
 
 
 if($_POST['choix']=='mobilier')
 {
		$connexion=getConnexionBD();
		$requete='SELECT * FROM Mobilier';
		$reponse=mysqli_query($connexion, $requete);
		if($reponse == FALSE){
			printf("<p>Un problème est survenu lors de la récupération des mobiliers.</p>");
		}
		else {
			echo '<h2>Liste des Mobiliers</h2><p><ul>';
			echo '<table border="1" width="400">';
			echo '<tr>';
			echo '<td>Id Element</td>';
			echo '<td>Nom Mobilier</td>';
			echo '<td>Déplaçable</td>';
			echo '</tr>';
			
			while ($row = mysqli_fetch_assoc($reponse)) {
				echo '<tr>';
				echo '<td>';
				echo  $row['IdElement'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['NomMobilier'] ;
				echo '</td>';
				echo '<td>';
				echo  $row['Deplacable'] ;
				echo '</td>';
				echo '</tr>';
			}
			echo '</ul></p>';
			echo '</table>';
			
		}
	 
 } 
}
?>

			
		</main>	
	</div>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <?php include('static/footer.php'); ?>
</body>
</html>
