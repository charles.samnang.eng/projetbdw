<html>

<head>
    <meta charset="utf-8" />
    <!-- lie le style CSS externe  -->
    <link href="css/style.css" rel="stylesheet" media="all" type="text/css">
    <!-- ajoute une image favicon (dans l'onglet du navigateur) -->
</head>
<body>
	<header><?php include('static/header.php'); ?></header>
    <br />
    <div id="divCentral">
		<?php include('static/menu.php'); ?>
		<main>
			<form method="post" action="">
				<p>
			<label for="titre">Titre de la carte : </label><input type="text" name="titre" value="Carte banale">
			<br/>
			
			<label for="description">Description de la carte : </label>
			
			<textarea name="description" rows="10" cols="50">Ecrivez une description de la carte.</textarea>
			<br/>
			<label for="environnement">Environnement : </label>
			<select name="environnement">
				<?php 
				$connexion=getConnexionBD();
				$requete='SELECT NomEnvironnement FROM Environnement'; 
				$reponse=mysqli_query($connexion, $requete);
				$nbEnvironnement=mysqli_num_rows($reponse); // Compte le nombre d'environnements
				$i=0;
				$donnees=mysqli_fetch_all($reponse, MYSQLI_ASSOC);
				foreach($donnees as $ligne)
				{
					$Environnement[$i]=$ligne['NomEnvironnement']; //Créer un tableau contenant les noms des environnements
					$i++;
				}
	
				for($i=1; $i<$nbEnvironnement; $i++)
				{
					echo "<option value=$Environnement[$i]>$Environnement[$i]</option>"; //créer les environnements disponibles a choisir		
				}
				?>
			</select>
			<br/>
			<label for="longueur">Longueur de la carte : </label><input type="number" name="longueur" value="10" min="0">
			<br/>
			<label for="largeur">Largeur de la carte :</label> <input type="number" name="largeur" value="10" min="0">
			<br/>
			<label for="equipement">Nombre d'équipement : </label>
			<label for="equipementMini">Mini : </label>
			<input type="number" name="equipementMini" min="0" value="5">
			<label for="equipementMaxi"> Maxi : </label>
			<input type="number" name="equipementMaxi" min="0" value="10"> 
			<br/>
			<label for="piege">Nombre de pièges : </label>
			<label for="piegeMini">Mini : </label>
			<input type="number" name="piegeMini" min="0" value="5">
			<label for="piegeMaxi">  Maxi : </label>
			<input type="number" name="piegeMaxi" min="0" value="10"> 
			<br/>
			<label for="mobiliers">Nombre de mobiliers : </label>
			<label for="mobilierMini">Mini : </label>
			<input type="number" name="mobilierMini" min="0" value="5">
			<label for="mobilierMaxi">  Maxi : </label>
			<input type="number" name="mobilierMaxi" min="0" value="10"> 
			<br/>
			<label for="creature">Nombre de créatures : </label>
			<label for="creatureMini">Mini : </label>
			<input type="number" name="creatureMini" min="0" value="5">
			<label for="creatureMaxi">  Maxi : </label>
			<input type="number" name="creatureMaxi" min="0" value="10"> 
			<br/>
			<label for="PNJ">Nombre de PNJ : </label>
			<label for="PNJMini">Mini : </label>
			<input type="number" name="PNJMini" min="0" value="5">
			<label for="PNJMaxi">  Maxi : </label>
			<input type="number" name="PNJMaxi" min="0" value="10"> 
			<br/>
			
			
			<input type="submit" value="Créer la carte" name="Submit" />
			
			</p>
			
</form>

<?php 

if(isset($_POST['Submit']))
{
$environnement=$_POST['environnement'];  //définit l'environnement reçu 
$titre=$_POST['titre'];
$taille=$_POST['longueur']*$_POST['largeur'];			//créer une variable $taille contenant la taille de la carte
$minElement=$_POST['equipementMini']+$_POST['piegeMini']+$_POST['mobilierMini']+$_POST['creatureMini']+$_POST['PNJMini'];	//créer une variable contenant la somme mini des éléments contenus dans la carte
$maxElement=$_POST['equipementMaxi']+$_POST['piegeMaxi']+$_POST['mobilierMaxi']+$_POST['creatureMaxi']+$_POST['PNJMaxi'];	//créer une variable contenant la somme maxi des éléments contenus dans la carte
$nbEquipement=rand($_POST['equipementMini'],$_POST['equipementMaxi']);	//Choisi un nombre aléatoire entre le mini et le maxi pour le nb d'équipement
$nbPiege=rand($_POST['piegeMini'], $_POST['piegeMaxi']);				//Choisi un nombre aléatoire entre le mini et le maxi pour le nb de pièges
$nbMobilier=rand($_POST['mobilierMini'], $_POST['mobilierMaxi']);		//Choisi un nombre aléatoire entre le mini et le maxi pour le nb de mobiliers
$nbCreature=rand($_POST['creatureMini'], $_POST['creatureMaxi']);		//Choisi un nombre aléatoire entre le mini et le maxi pour le nb de créatures
$nbPNJ=rand($_POST['PNJMini'], $_POST['PNJMaxi']);						//Choisi un nombre aléatoire entre le mini et le maxi pour le nb de PNJ
$connexion=getConnexionBD();

$requete="SELECT * FROM Equipement";				//Créer une variable contenant le nombre d'équipement dans la BDD
$nb_ligne=mysqli_query($connexion, $requete);
$numEquipement=mysqli_num_rows($nb_ligne);

$requete="SELECT * FROM Piege";						//Créer une variable contenant le nombre de pièges dans la BDD
$nb_ligne=mysqli_query($connexion, $requete);		
$numPiege=mysqli_num_rows($nb_ligne);


$requete="SELECT * FROM Mobilier";					//Créer une variable contenant le nombre de mobiliers dans la BDD
$nb_ligne=mysqli_query($connexion, $requete);		
$numMobilier=mysqli_num_rows($nb_ligne);

$requete="SELECT * FROM Creature NATURAL JOIN Environnement WHERE NomEnvironnement='$environnement'";					//Créer une variable contenant le nombre de créatures dans la BDD
$nb_ligne=mysqli_query($connexion, $requete);		
$numCreature=mysqli_num_rows($nb_ligne);


$requete="SELECT * FROM PNJ";						//Créer une variable contenant le nombre de PNJ dans la BDD
$nb_ligne=mysqli_query($connexion, $requete);		
$numPNJ=mysqli_num_rows($nb_ligne);


//echo $numPNJ,$numMobilier, $numCreature, $numPiege,$numEquipement;

$requete="SELECT * FROM Creature NATURAL JOIN Environnement WHERE NomEnvironnement='$environnement'";  //Créer un tableau $nomCreature contenant le nom des créatures
$reponse=mysqli_query($connexion, $requete);
$donnees=mysqli_fetch_all($reponse, MYSQLI_ASSOC);
$i=0;
foreach($donnees as $ligne)
{
	$nomCreature[$i]=$ligne['NomCreature'];
	$i++;
}

$requete='SELECT NomElement FROM Equipement NATURAL JOIN Element';	//Créer un tableau $nomEquipement contenant le nom des équipements
$reponse=mysqli_query($connexion, $requete);
$donnees=mysqli_fetch_all($reponse, MYSQLI_ASSOC);
$i=0;
foreach($donnees as $ligne)
{
	$nomEquipement[$i]=$ligne['NomElement'];
	$i++;
}

$requete='SELECT NomElement FROM Piege NATURAL JOIN Element'; 	//Créer un tableau $nomPiege contenant le nom des pièges
$reponse=mysqli_query($connexion, $requete);
$donnees=mysqli_fetch_all($reponse, MYSQLI_ASSOC);
$i=0;
foreach($donnees as $ligne)
{
	$nomPiege[$i]=$ligne['NomElement'];
	$i++;
}

$requete='SELECT NomElement FROM Mobilier NATURAL JOIN Element';	//Créer un tableau $nomMobilier contenant le nom des mobiliers
$reponse=mysqli_query($connexion, $requete);
$donnees=mysqli_fetch_all($reponse, MYSQLI_ASSOC);
$i=0;
foreach($donnees as $ligne)
{
	$nomMobilier[$i]=$ligne['NomElement'];
	$i++;
}

$requete='SELECT NomPNJ FROM PNJ';									//Créer un tableau $nomPNJ contenant le nom des PNJ
$reponse=mysqli_query($connexion, $requete);
$donnees=mysqli_fetch_all($reponse, MYSQLI_ASSOC);
$i=0;
foreach($donnees as $ligne)
{
	$nomPNJ[$i]=$ligne['NomPNJ'];
	$i++;
}



if($minElement<=$taille && $maxElement<=$taille && $minElement<=$maxElement ) //Vérifie que mini des éléments < maxi et qu'ils sont les 2 inférieurs a la taille de la carte
{
	$carte=array(array());	//Création d'une table vide 
	
	for($i=0; $i<=$_POST['longueur']-1; $i++) //Remplissage de le carte de cases vides
	{
		for($j=0; $j<=$_POST['largeur']-1;$j++)
		{
			$carte[$i][$j]="img/sol.png";			//Crée un tableau de la carte ne contenant que le sol pour l'instant
			$label[$i][$j]="Sol";					// Crée un tableau de label ne contenant que des "Sol" pour l'instant
		}
	}
	
	if($numEquipement>0)
	{
		while($nbEquipement>0) //Remplissage des équipements dans la carte
		{
			$x=rand(0,$_POST['longueur']-1);
			$y=rand(0, $_POST['largeur']-1);
			if($carte[$x][$y]=="img/sol.png")		//Vérifie qu'on est bien sur une case ne contenant aucun élément
			{
				$ind=rand(0,$numEquipement-1);
				$carte[$x][$y]="img/equipement.png";
				$label[$x][$y]=$nomEquipement[$ind];
				$nbEquipement--;
			}
		}
	} else echo "Aucun équipement dans la base de données ! <br/>";
	
	
	if($numPiege>0)
	{
		while($nbPiege>0) //Remplissage des Pièges dans la carte
		{
			$x=rand(0,$_POST['longueur']-1);
			$y=rand(0, $_POST['largeur']-1);
			if($carte[$x][$y]=="img/sol.png")		//Vérifie qu'on est bien sur une case ne contenant aucun élément
			{
				$ind=rand(0,$numPiege-1);
				$carte[$x][$y]="img/piege.png";
				$label[$x][$y]=$nomPiege[$ind];
				$nbPiege--;
			}
		}
	} else echo "Aucun piège dans la base de données ! <br />";
	
	if($numMobilier>0)
	{
		while($nbMobilier>0) //Remplissage des Mobiliers dans la carte
		{
			$x=rand(0,$_POST['longueur']-1);
			$y=rand(0, $_POST['largeur']-1);
			if($carte[$x][$y]=="img/sol.png")		//Vérifie qu'on est bien sur une case ne contenant aucun élément
			{
				$ind=rand(0,$numMobilier-1);
				$carte[$x][$y]="img/mobilier.png";
				$label[$x][$y]=$nomMobilier[$ind];
				$nbMobilier--;
			}
		}
	}
	else echo "Aucun mobilier dans la base de données ! <br />";

	if($numCreature>0)
	{
		while($nbCreature>0) //Remplissage des Créatures dans la carte
		{
			$x=rand(0,$_POST['longueur']-1);
			$y=rand(0, $_POST['largeur']-1);
			if($carte[$x][$y]=="img/sol.png")		//Vérifie qu'on est bien sur une case ne contenant aucun élément
			{
				$ind=rand(0,$numCreature-1);
				$carte[$x][$y]="img/creature.png";
				$label[$x][$y]=$nomCreature[$ind];
				$nbCreature--;
			}
		}
	}
	else echo "Aucune créature dans la base de données ! <br />";
	
	
	if($numPNJ>=$nbPNJ)				// Vérifie que il y a assez de PNJ dans la base de données
	{
		while($nbPNJ>0) //Remplissage des PNJ dans la carte
		{
			$x=rand(0,$_POST['longueur']-1);
			$y=rand(0, $_POST['largeur']-1);
			if($carte[$x][$y]=="img/sol.png")
			{
				$ind=rand(0,$numPNJ-1);
				$carte[$x][$y]="img/PNJ.png";
				$label[$x][$y]=$nomPNJ[$ind];
				while($nomPNJ[$ind]=="Vide")   //Vérifie qu'on ne récupère pas une case "Vide" du tableau des nom de PNJ
				{
					$ind=rand(0,$numPNJ-1); 
				}
				$label[$x][$y]=$nomPNJ[$ind];
				$nomPNJ[$ind]="Vide";  // "Vide" le tableau contenant le nom des PNJ afin de ne pas reprendre le même
				$nbPNJ--;
			}
		}
	}
	else echo "Pas assez de PNJ dans la base de données";
	
		echo "<h1>$titre</h1>";
		echo '<table border="2">';  //Affichage de la carte sous forme de tableau
		for ($i=1; $i<=$_POST['longueur']; $i++) {
		   echo '<tr>';
		   for ($j=1; $j<=$_POST['largeur']; $j++) {
				 echo '<td>';
				  // ------------------------------------------
				  // AFFICHAGE ligne $i, colonne $j
				  
				 // echo $carte["$i"-1]["$j"-1];
				  
				 $image=$carte["$i"-1]["$j"-1];				//Permet d'affectere l'image à la case du tableau associé
				 $lab=$label["$i"-1]["$j"-1];				//Permet d'affecter le titre de l'image au label associé
				echo "<img src='$image' height='50' width='50' title='$lab'/>";		//affiche l'image contenue dans la variable $image
				  // ------------------------------------------
				 echo '</td>';
		   }
		   echo '</tr>';
		   $j=1;
		}
		echo '</table>';
		echo "<br/>";
		echo "<br/>";
		echo "<br/>";
		echo '<h2>Légende</h2>';
		echo '<table border="1">';  //Affichage de la Légende
		echo '<tr>';
			echo '<td>Case vide</td>';
			echo '<td>PNJ</td>';
			echo '<td>Créature</td>';
			echo '<td>Equipement</td>';
			echo '<td>Mobilier</td>';
			echo '<td>Piège</td>';
		echo '</tr>';
		echo '<tr>';
		echo "<td><img src='img/sol.png' height='50' width='50'/></td>";
		echo "<td><img src='img/PNJ.png' height='50' width='50'/></td>";
		echo "<td><img src='img/creature.png' height='50' width='50'/></td>";
		echo "<td><img src='img/equipement.png' height='50' width='50'/></td>";
		echo "<td><img src='img/mobilier.png' height='50' width='50'/></td>";
		echo "<td><img src='img/piege.png' height='50' width='50'/></td>";
		echo '</tr>';
		echo '</table>';
	
}

else
{
	echo "Erreur dans le nombre d'élement de la carte";
}	
}
?>
		</main>	
	</div>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <?php include('static/footer.php'); ?>
</body>




</html>
