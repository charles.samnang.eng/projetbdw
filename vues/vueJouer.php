<!DOCTYPE html>
<!-- 
Page d'accueil non MVC. En début de TP, on vous demande de la modifier directement, puis vous la transformerez selon l'architecture MVC.
-->
<html>
<head>
    <meta charset="utf-8" />
    <!-- lie le style CSS externe  -->
    <link href="css/style.css" rel="stylesheet" media="all" type="text/css">
    <!-- ajoute une image favicon (dans l'onglet du navigateur) -->
</head>
<body>
	<header><?php include('static/header.php'); ?></header>
    <br />
    <div id="divCentral">
		<?php include('static/menu.php'); ?>
		<main>
			<p id="jouer">Pour commencer à jouer, c'est ici !</p>

		</main>	
	</div>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <?php include('static/footer.php'); ?>
</body>
</html>
