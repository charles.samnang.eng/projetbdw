<!DOCTYPE html>
<!-- 
Page d'accueil non MVC. En début de TP, on vous demande de la modifier directement, puis vous la transformerez selon l'architecture MVC.
-->
<html>
<head>
    <meta charset="utf-8" />
    <!-- lie le style CSS externe  -->
    <link href="css/style.css" rel="stylesheet" media="all" type="text/css">
    <!-- ajoute une image favicon (dans l'onglet du navigateur) -->
</head>
<body>
	<header><?php include('static/header.php'); ?></header>
    <br />
    <div id="divCentral">
		<?php include('static/menu.php'); ?>
		<main>
			<p>Fant'easy est un jeu de rôle et de plateau, donc le but est de soit blablabla</p>
            <br />
            <p>Qu'est-ce qu'un jeu de rôle ?</p>
            <p>Le jeu de rôle est une activité de loisir et de divertissement dont le but principal est d’endosser l’identité d’un personnage fictif le temps d’une partie. Ainsi, installés autour d’une table, les joueurs évoquent ensemble un univers virtuel dans lequel ils décrivent les aventures des personnages qu’ils interprètent, chacun jouant le rôle de l’un d’entre eux.</p>
		</main>	
	</div>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <?php include('static/footer.php'); ?>
</body>
</html>
