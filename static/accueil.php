<!DOCTYPE html>
<!-- 
Page d'accueil non MVC. En début de TP, on vous demande de la modifier directement, puis vous la transformerez selon l'architecture MVC.
-->
<html>
<head>
    <meta charset="utf-8" />
    <!-- lie le style CSS externe  -->
    <link href="css/style.css" rel="stylesheet" media="all" type="text/css">
    <!-- ajoute une image favicon (dans l'onglet du navigateur) -->
</head>
<body>
	<header><?php include('header.php'); ?></header>
    <br />
    <div id="divCentral">
		<?php include('menu.php'); ?>
		<main>
			<p>Salutation visiteur, bienvenue sur Fant'Easy !</p>
			<img src="img/jdr.jpg" height="50%" width="50%" alt="Image jeu de role" class="ImgJdr">
		</main>	
	</div>
    <?php include('footer.php'); ?>
</body>
</html>
